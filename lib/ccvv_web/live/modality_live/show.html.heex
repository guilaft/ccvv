<%= if @live_action in [:edit] do %>
  <.modal return_to={Routes.modality_show_path(@socket, :show, @modality)}>
    <.live_component
      module={CCVVWeb.ModalityLive.FormComponent}
      id={@modality.id}
      title={@page_title}
      action={@live_action}
      modality={@modality}
      return_to={Routes.modality_show_path(@socket, :show, @modality)}
    />
  </.modal>
<% end %>

<%= if @live_action in [:add_people] do %>
  <.modal return_to={Routes.modality_show_path(@socket, :show, @modality)}>
    <.live_component
      module={CCVVWeb.ModalityLive.AddPeopleComponent}
      id="add_people_component"
      people={@avaliable_people_in_search -- @selected_people}
      search_query={@search_query}
      return_to={Routes.modality_show_path(@socket, :show, @modality)}
    />

  <div class="box">
    <h3 class="title is-4 text-center"><%= gettext "Selected People" %></h3>
    <div class="flex flex-row flex-wrap">
      <%= for person <- @selected_people do %>
          <div class="flex py-4 px-4 mx-2 bg-white rounded shadow cursor-pointer hover:bg-gray-100"
               phx-click="deselect_person"
               phx-value-person-id={person.id}>
            <%= person.first_name %>

            <span class="icon ml-2">
              <.icon name="x-circle" class="icon"/>
            </span>
          </div>
    <% end %>
    </div>
  </div>
  </.modal>
<% end %>

<nav class="breadcrumb" aria-label="breadcrumbs">
  <ul>
    <li>
      <%= live_redirect gettext("Modalities"), to: Routes.modality_index_path(@socket, :index) %>
    </li>
    <li class="is-active"><a href="#" aria-current="page"><%= @modality.id %></a></li>
  </ul>
</nav>

<div class="card">
  <div class="card-content">
    <div class="media">
      <div class="media-left">
        <figure class="image is-96x96">
          <img class="" src="https://icon-library.com/images/class-icon-png/class-icon-png-15.jpg" alt="Placeholder image">
        </figure>
      </div>
      <div class="media-content">
        <p class="title is-4"><%= @modality.name %></p>
      </div>
    </div>
  </div>

  <div class="card-content">
    <%= @modality.description %>
  </div>

  <footer class="card-footer">
   <%= live_patch gettext("Edit"), to: Routes.modality_show_path(@socket, :edit, @modality), class: "card-footer-item" %>
   <%= link gettext("Delete"), to: "#", phx_click: "delete", phx_value_modality_id: @modality.id, data: [confirm: "Are you sure?"], class: "card-footer-item" %>
  </footer>
</div>

<section class="box mt-2">
  <div class="block">
    <span class="title"><%= gettext("People Participating List") %></span>
  </div>

  <div class="navbar">
    <.live_component module={CCVVWeb.Components.FilterComponent} id="filter" filter={@filter} />

    <div class="navbar-end">
      <%= live_patch gettext("Add People"), to: Routes.modality_show_path(@socket, :add_people, @modality), class: "button is-link" %>
    </div>
  </div>

  <table class="table is-fullwidth is-striped is-bordered">
    <thead>
      <tr>
        <th>
          <%= sorting_link(@sorting, gettext("ID"), :id) %>
        </th>
        <th>
          <%= sorting_link(@sorting, gettext("First name"), :first_name) %>
        </th>
        <th>
          <%= sorting_link(@sorting, gettext("Last name"), :last_name) %>
        </th>
        <th>
          <%= gettext("Email") %>
        </th>
        <th>
          <%= gettext("Phone Number") %>
        </th>
        <th>
          <%= gettext("Details") %>
        </th>
        <th>
          <%= gettext("Actions") %>
        </th>
      </tr>
    </thead>
    <tbody id="people">
      <%= if length(@modality.people) < 1 do %>
          <tr class="notification text-center">
            <td colspan="100"><%= gettext "No matching records found" %></td>
          </tr>
          <% end %>

          <%= for person <- @modality.people do %>
              <tr class="" id={"person-#{person.id}"}>
                <td><%= person.id %></td>
                <td><%= person.first_name %></td>
                <td><%= person.last_name %></td>
                <td><%= person.email %></td>
                <td><%= person.phone_number %></td>
                <td>
                  <p class="w-40 truncate text-ellipse"><%= person.details %></p>
                </td>

                <td class="">
                  <button class="w-5" title={gettext("Show Person")}>
                    <%= live_redirect to: Routes.person_show_path(@socket, :show, person) do %>
                    <.icon name="arrow-uturn-right" />
                    <% end %>
                  </button>
                  <button class="w-5" title={gettext("Remove Person From Modality")}>
                    <%= link to: "#", phx_click: "remove_person_from_modality", phx_value_person_id: person.id, data: [confirm: "Are you sure?"], id: "person-delete-button" do %>
                    <.icon name="backspace" />
                    <% end %>
                  </button>
                </td>
              </tr>
          <% end %>
    </tbody>
  </table>

  <.live_component module={CCVVWeb.Components.PaginationComponent} id="pagination" pagination={@pagination} />
</section>
