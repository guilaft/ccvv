defmodule CCVV.UsersTest do
  use CCVV.DataCase

  alias CCVV.Users

  describe "people" do
    alias CCVV.Users.Person

    import CCVV.UsersFixtures

    @invalid_attrs %{details: nil, email: nil, first_name: nil, last_name: nil, phone_number: nil}

    test "list_people/0 returns all people" do
      person = person_fixture()
      assert Users.list_people() == [person]
    end

    test "get_person!/1 returns the person with given id" do
      person = person_fixture()
      assert Users.get_person!(person.id) == person
    end

    test "create_person/1 with valid data creates a person" do
      valid_attrs = %{
        details: "some details",
        email: "some@email.com",
        first_name: "some first_name",
        last_name: "some last_name",
        phone_number: "21912345678"
      }

      assert {:ok, %Person{} = person} = Users.create_person(valid_attrs)
      assert person.details == "some details"
      assert person.email == "some@email.com"
      assert person.first_name == "some first_name"
      assert person.last_name == "some last_name"
      assert person.phone_number == "+55 21 91234-5678"
    end

    test "create_person/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.create_person(@invalid_attrs)
    end

    test "update_person/2 with valid data updates the person" do
      person = person_fixture()
      update_attrs = %{
        details: "some updated details",
        email: "some_updated@email.com",
        first_name: "some updated first_name",
        last_name: "some updated last_name",
        phone_number: "21967746292"
      }

      assert {:ok, %Person{} = person} = Users.update_person(person, update_attrs)

      assert person.details == "some updated details"
      assert person.email == "some_updated@email.com"
      assert person.first_name == "some updated first_name"
      assert person.last_name == "some updated last_name"
      assert person.phone_number == "+55 21 96774-6292"
    end

    test "update_person/2 with invalid data returns error changeset" do
      person = person_fixture()
      assert {:error, %Ecto.Changeset{}} = Users.update_person(person, @invalid_attrs)
      assert person == Users.get_person!(person.id)
    end

    test "delete_person/1 deletes the person" do
      person = person_fixture()
      assert {:ok, %Person{}} = Users.delete_person(person)
      assert_raise Ecto.NoResultsError, fn -> Users.get_person!(person.id) end
    end

    test "change_person/1 returns a person changeset" do
      person = person_fixture()
      assert %Ecto.Changeset{} = Users.change_person(person)
    end
  end
end
