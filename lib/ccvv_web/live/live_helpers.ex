defmodule CCVVWeb.LiveHelpers do
  import Phoenix.LiveView
  import Phoenix.LiveView.Helpers
  use Phoenix.Component

  alias Phoenix.LiveView.JS

  @doc """
  Renders a live component inside a modal.

  The rendered modal receives a `:return_to` option to properly update
  the URL when the modal is closed.

  ## Examples

      <.modal return_to={Routes.person_index_path(@socket, :index)}>
        <.live_component
          module={CCVVWeb.PersonLive.FormComponent}
          id={@person.id || :new}
          title={@page_title}
          action={@live_action}
          return_to={Routes.person_index_path(@socket, :index)}
          person: @person
        />
      </.modal>
  """
  def modal(assigns) do
    assigns = assign_new(assigns, :return_to, fn -> nil end)

    ~H"""
    <div id="modal" class="modal is-active fade-in" phx-remove={hide_modal()}>
      <div class="modal-background"></div>

      <div
        id="modal-content"
        class="modal-content bg-white rounded-md p-2"
        phx-click-away={JS.dispatch("click", to: "#close")}
        phx-window-keydown={JS.dispatch("click", to: "#close")}
        phx-key="escape"
      >
        <%= if @return_to do %>
          <%= live_patch "",
            to: @return_to,
            id: "close",
            class: "phx-modal-close delete",
            phx_click: hide_modal()
          %>
        <% else %>
          <a id="close" href="#" class="phx-modal-close delete" phx-click={hide_modal()}></a>
        <% end %>

        <%= render_slot(@inner_block) %>
      </div>
    </div>
    """
  end

  defp hide_modal(js \\ %JS{}) do
    js
    |> JS.hide(to: "#modal", transition: "fade-out")
    |> JS.hide(to: "#modal-content", transition: "fade-out-scale")
  end


  def flash(%{kind: :error} = assigns) do
    ~H"""
    <%= if live_flash(@flash, @kind) do %>
      <div
        id="flash"
        class="notification is-danger is-light fixed"
        phx-click={JS.push("lv:clear-flash") |> JS.remove_class("fade-in-scale") |> hide("#flash")}
        phx-value-key="info"
        phx-hook="Flash"
      >
          <%= live_flash(@flash, @kind) %>

          <button type="button" class="delete"></button>
      </div>
    <% end %>
    """
  end

  def flash(%{kind: :info} = assigns) do
    ~H"""
    <%= if live_flash(@flash, @kind) do %>
      <div
        id="flash"
        class="notification is-info is-light fixed"
        phx-click={JS.push("lv:clear-flash") |> JS.remove_class("fade-in-scale") |> hide("#flash")}
        phx-value-key="info"
        phx-hook="Flash"
      >
          <%= live_flash(@flash, @kind) %>

          <button type="button" class="delete"></button>
      </div>
    <% end %>
    """
  end

  def icon(assigns) do
      ~H"""
      <Heroicons.LiveView.icon name={@name} type="outline" class="h-4 w-4" />
      """
  end

  def hide(js \\ %JS{}, selector) do
    JS.hide(js,
      to: selector,
      time: 300,
      transition:
        {"transition ease-in duration-300", "transform opacity-100 scale-100",
         "transform opacity-0 scale-95"}
    )
  end

  def toggle_active(id, js \\ %JS{}) do
    js
    |> JS.remove_class(
      "is-active",
    to: ".is-active"
    )
    |> JS.add_class(
      "is-active",
    to: "#{id}:not(.is-active)"
    )
  end

  def sorting_link(sorting, text, key) do
    live_component %{
      module: CCVVWeb.Components.SortingComponent,
      id: "sorting-#{key}",
      label: text,
      key: key,
      sorting: sorting,
    }
  end
end
