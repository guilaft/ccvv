<section class="section">
  <h1 class="title"><%= gettext "Listing Modalities" %></h1>
</section>

<%= if @live_action in [:new, :edit] do %>
  <.modal return_to={Routes.modality_index_path(@socket, :index)}>
    <.live_component
      module={CCVVWeb.ModalityLive.FormComponent}
      id={@modality.id || :new}
      title={@page_title}
      action={@live_action}
      modality={@modality}
      return_to={Routes.modality_index_path(@socket, :index)}
    />
  </.modal>
<% end %>


<div class="box">
  <div class="navbar">
    <div class="navbar-end">
      <%= live_patch gettext("New Modality"), to: Routes.modality_index_path(@socket, :new), class: "button is-link"%>
    </div>
  </div>

  <div class="flex md:flex-row md:flex-wrap flex-col" id="modalities">
    <%= if length(@modalities) < 1 do %>
        <div class="box text-center w-full bg-white">
          <span class="text-2xl "><%= gettext "No modalities found. Try creating one!" %></span>
        </div>
    <% end %>

    <%= for modality <- @modalities do %>
        <div class="p-2 m-2 shadow-md rounded-md min-w-full md:min-w-0 md:w-1/4 h-40" id={"modality-#{modality.id}"}>
          <div class="navbar">
            <%= live_redirect modality.name, to: Routes.modality_show_path(@socket, :show, modality), class: "title is-4" %>

            <div class="navbar-end">
              <.dropdown socket={@socket} modality={modality} />
            </div>
          </div>

          <p class="content overflow-hidden whitespace-pre-wrap max-h-[50%]"><%= modality.description %></p>
        </div>
    <% end %>
  </div>
</div>
