defmodule CCVVWeb.PageController do
  use CCVVWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
