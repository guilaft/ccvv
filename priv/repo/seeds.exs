# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     CCVV.Repo.insert!(%CCVV.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias NimbleCSV.RFC4180, as: CSV
alias CCVV.Repo
alias CCVV.Users
alias CCVV.Modalities

"priv/repo/us-500.csv"
|> File.stream!(read_ahead: 100_000)
|> CSV.parse_stream
|> Stream.map(fn [first_name, last_name, email, phone_number] ->
  Repo.insert!(%Users.Person{
        first_name: first_name,
        last_name: last_name,
        phone_number: phone_number,
        email: email
})
end)
|> Stream.run

{:ok, modality} = Modalities.create_modality(%{name: "Jiu-Jitsu", description: "Aulas de Jiu-Jitsu até 18 anos de idade."})

for id <- 1..10 do
    person = Users.get_person!(id)
    Modalities.add_person_to_modality(modality, person)
end
