defmodule CCVV.Repo do
  use Ecto.Repo,
    otp_app: :ccvv,
    adapter: Ecto.Adapters.Postgres
end
