defmodule CCVV.Users do
  @moduledoc """
  The Users context.
  """

  import Ecto.Query, warn: false
  alias CCVV.Repo

  alias CCVV.Users.Person

  @doc """
  Returns the list of people.

  ## Examples

      iex> list_people()
      [%Person{}, ...]

  """
  def list_people do
    Repo.all(Person)
  end

  @doc """
  Returns the list of people matching the following `criteria`

  ## Example Criteria
  [
    paginate: %{page: 1, per_page: 5},
    sort: %{sort_by: :item, sort_order: :asc}
  ]
  """
  def list_people(criteria) when is_list(criteria) do
    Person.Query.list_all(criteria)
    |> Repo.all()
  end

  def list_people_with_total_count(opts \\ []) do
    query = Person.Query.filter(opts)

    total_count = Repo.aggregate(query, :count)

    result =
      query
      |> Person.Query.sort(opts)
      |> Person.Query.paginate(opts)
      |> Repo.all()

    %{people: result, total_count: total_count}
  end

  @doc """
  Gets a single person.

  Raises `Ecto.NoResultsError` if the Person does not exist.

  ## Examples

      iex> get_person!(123)
      %Person{}

      iex> get_person!(456)
      ** (Ecto.NoResultsError)

  """
  def get_person!(id), do: Repo.get!(Person, id)

  @doc """
  Creates a person.

  ## Examples

      iex> create_person(%{field: value})
      {:ok, %Person{}}

      iex> create_person(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_person(attrs \\ %{}) do
    %Person{}
    |> Person.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a person.

  ## Examples

      iex> update_person(person, %{field: new_value})
      {:ok, %Person{}}

      iex> update_person(person, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_person(%Person{} = person, attrs) do
    person
    |> Person.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a person.

  ## Examples

      iex> delete_person(person)
      {:ok, %Person{}}

      iex> delete_person(person)
      {:error, %Ecto.Changeset{}}

  """
  def delete_person(%Person{} = person) do
    Repo.delete(person)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking person changes.

  ## Examples

      iex> change_person(person)
      %Ecto.Changeset{data: %Person{}}

  """
  def change_person(%Person{} = person, attrs \\ %{}) do
    Person.changeset(person, attrs)
  end
end
