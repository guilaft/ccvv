defmodule CCVVWeb.Components.FilterComponent do
  use CCVVWeb, :live_component

  alias CCVVWeb.Forms.FilterForm

  def render(assigns) do
    ~H"""
    <div>
      <.form let={f} id="table-filter" for={@changeset} as="filter" phx-submit="search" phx-target={@myself} >
        <div>
          <div class="field is-grouped is-horizontal">
            <div class="field-body">
              <div class="field is-expanded">
                <p class="control">
                  <%= text_input f, :first_name, class: "input", placeholder: gettext("First name") %>
                </p>
                <p class="help is-danger">
                  <%= error_tag f, :first_name %>
                </p>
              </div>

              <div class="field">
                <p class="control">
                  <%= text_input f, :last_name, class: "input", placeholder: gettext("Last name") %>
                </p>
                <p class="help is-danger">
                  <%= error_tag f, :last_name %>
                </p>
              </div>
                <p class="control">
                  <%= submit gettext("Search"), class: "button is-link" %>
                </p>
            </div>
          </div>
        </div>
      </.form>
    </div>
    """
  end

  def update(assigns, socket) do
    {:ok, assign_changeset(assigns, socket)}
  end

  def handle_event("search", %{"filter" => filter}, socket) do
    case FilterForm.parse(filter) do
      {:ok, opts} ->
        send(self(), {:update, opts})
        {:noreply, socket}

      {:error, changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp assign_changeset(%{filter: filter}, socket) do
    assign(socket, :changeset, FilterForm.change_values(filter))
  end
end
