defmodule CCVVWeb.ModalityLive.AddPeopleComponent do
  use CCVVWeb, :live_component

  alias CCVV.Users

  def handle_event("select-person", %{"person-id" => person_id}, socket) do
    person = Users.get_person!(person_id)

    send self(), {:person_selected, person}
    {:noreply, socket}
  end
end
