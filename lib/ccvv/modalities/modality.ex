defmodule CCVV.Modalities.Modality do
  use Ecto.Schema
  import Ecto.Changeset

  alias CCVV.Modalities.PeopleModalities

  schema "modalities" do
    field :description, :string
    field :name, :string
    field :people_count, :integer, virtual: true

    many_to_many :people, CCVV.Users.Person, join_through: PeopleModalities

    timestamps()
  end

  @doc false
  def changeset(modality, attrs) do
    modality
    |> cast(attrs, [:name, :description])
    |> validate_required([:name])
  end
end
