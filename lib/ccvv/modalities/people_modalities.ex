defmodule CCVV.Modalities.PeopleModalities do
  use Ecto.Schema
  import Ecto.Changeset

  alias CCVV.Modalities.Modality
  alias CCVV.Modalities.PeopleModalities
  alias CCVV.Users.Person

  @primary_key false
  schema "people_modalities" do
    belongs_to :person, Person, on_replace: :delete
    belongs_to :modality, Modality, on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(%PeopleModalities{} = people_modalities, attrs) do
    people_modalities
    |> cast(attrs, [:person_id, :modality_id])
    |> validate_required([:person_id, :modality_id])
    |> unique_constraint(:person, name: :person_id_modality_id_unique_index)
  end
end
