defmodule CCVV.ModalitiesTest do
  use CCVV.DataCase

  alias CCVV.Modalities

  describe "modalities" do
    alias CCVV.Modalities.Modality

    import CCVV.ModalitiesFixtures
    import CCVV.UsersFixtures

    @invalid_attrs %{description: nil, name: nil}

    test "list_modalities/0 returns all modalities" do
      modality = modality_fixture()
      assert Modalities.list_modalities() == [modality]
    end

    test "get_modality!/1 returns the modality with given id" do
      modality = modality_fixture()
      assert Modalities.get_modality!(modality.id) == modality
    end

    test "get_modality_with_people_total_count!/2 returns modality with people list and total people" do
      modality = modality_fixture()
      total_people_count = 10

      for _index <- 1..total_people_count do
        Modalities.add_person_to_modality(modality, person_fixture())
      end

      %{modality: _modality, total_people_count: people_count} = Modalities.get_modality_with_people_total_count!(modality.id)

      assert people_count == total_people_count
    end

    test "add_person_to_modality/2 adds given person to modality" do
      person = person_fixture()
      modality = modality_fixture()

      Modalities.add_person_to_modality(modality, person)

      assert Modalities.get_modality_with_people!(modality.id).people == [person]
    end

    test "remove_person_from_modality/2 removes given person from modality" do
      person = person_fixture()
      modality = modality_fixture()

      Modalities.add_person_to_modality(modality, person)

      assert Modalities.get_modality_with_people!(modality.id).people == [person]

      Modalities.remove_person_from_modality(modality, person)

      assert Modalities.get_modality_with_people!(modality.id).people == []
    end

    test "create_modality/1 with valid data creates a modality" do
      valid_attrs = %{description: "some description", name: "some name"}

      assert {:ok, %Modality{} = modality} = Modalities.create_modality(valid_attrs)
      assert modality.description == "some description"
      assert modality.name == "some name"
    end

    test "create_modality/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Modalities.create_modality(@invalid_attrs)
    end

    test "update_modality/2 with valid data updates the modality" do
      modality = modality_fixture()
      update_attrs = %{description: "some updated description", name: "some updated name"}

      assert {:ok, %Modality{} = modality} = Modalities.update_modality(modality, update_attrs)
      assert modality.description == "some updated description"
      assert modality.name == "some updated name"
    end

    test "update_modality/2 with invalid data returns error changeset" do
      modality = modality_fixture()
      assert {:error, %Ecto.Changeset{}} = Modalities.update_modality(modality, @invalid_attrs)
      assert modality == Modalities.get_modality!(modality.id)
    end

    test "delete_modality/1 deletes the modality" do
      modality = modality_fixture()
      assert {:ok, %Modality{}} = Modalities.delete_modality(modality)
      assert_raise Ecto.NoResultsError, fn -> Modalities.get_modality!(modality.id) end
    end

    test "change_modality/1 returns a modality changeset" do
      modality = modality_fixture()
      assert %Ecto.Changeset{} = Modalities.change_modality(modality)
    end
  end
end
