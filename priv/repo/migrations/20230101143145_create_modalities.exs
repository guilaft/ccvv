defmodule CCVV.Repo.Migrations.CreateModalities do
  use Ecto.Migration

  def change do
    create table(:modalities) do
      add :name, :string
      add :description, :text

      timestamps()
    end
  end
end
