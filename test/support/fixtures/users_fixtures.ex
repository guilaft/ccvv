defmodule CCVV.UsersFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `CCVV.Users` context.
  """

  @doc """
  Generate a person.
  """
  def person_fixture(attrs \\ %{}) do
    {:ok, person} =
      attrs
      |> Enum.into(%{
        details: "some details",
        email: "some@email.com",
        first_name: "some first_name",
        last_name: "some last_name",
        phone_number: "21993651053"
      })
      |> CCVV.Users.create_person()

    person
  end
end
