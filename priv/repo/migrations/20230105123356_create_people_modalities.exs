defmodule CCVV.Repo.Migrations.CreatePeopleModalities do
  use Ecto.Migration

  def change do
    create table(:people_modalities) do
      add :person_id, references(:people, on_delete: :delete_all)
      add :modality_id, references(:modalities, on_delete: :delete_all)

      timestamps()
    end

    create index(:people_modalities, [:person_id])
    create index(:people_modalities, [:modality_id])

    create unique_index(
      :people_modalities, [:person_id, :modality_id], name: :person_id_modality_id_unique_index
    )
  end
end
