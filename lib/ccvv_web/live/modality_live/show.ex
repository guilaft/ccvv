defmodule CCVVWeb.ModalityLive.Show do
  use CCVVWeb, :live_view

  alias CCVV.Modalities
  alias CCVV.Users

  alias CCVVWeb.Forms.PaginationForm
  alias CCVVWeb.Forms.FilterForm
  alias CCVVWeb.Forms.SortingForm

  @impl true
  def mount(_params, _session, socket) do
    socket =
      socket
      |> assign(:selected_people, [])
      |> assign(:avaliable_people_in_search, [])
      |> assign(:search_query, "")

    {:ok, socket}
  end

  @impl true
  def handle_params(params, _, socket) do
    socket =
      socket
      |> parse_params(params)
      |> assign_modality(params)
      |> push_event("phx:page-loading-stop", %{})
      |> assign(:page_title, page_title(socket.assigns.live_action))

    {:noreply, socket}
  end

  @impl true
  def handle_event("delete", %{"modality_id" => id}, socket) do
    modality = Modalities.get_modality!(id)

    Modalities.delete_modality(modality)

    {:noreply, redirect(socket, to: Routes.modality_index_path(socket, :index))}
  end

  @impl true
  def handle_event("remove_person_from_modality", %{"person-id" => person_id}, socket) do
    person = Users.get_person!(person_id)
    modality = Modalities.get_modality!(socket.assigns.modality.id)

    {_entries, _deleted} = Modalities.remove_person_from_modality(modality, person)

    send(self(), {:update, %{}})

    {:noreply, socket}
  end

  @impl true
  def handle_event("search_people", %{"value" => ""}, socket) do
    {:noreply, assign(socket, :avaliable_people_in_search, [])}
  end

  @impl true
  def handle_event("search_people", %{"value" => value}, socket) do
    %{people: avaliable_people_in_search} =
      Users.list_people_with_total_count(%{first_name: value, page_size: 5, page: 1 })

    {:noreply, assign(socket, :avaliable_people_in_search, avaliable_people_in_search) |> assign(:search_query, value)}
  end

  @impl true
  def handle_event("deselect_person", %{"person-id" => person_id}, socket) do
    person = Users.get_person!(person_id)

    socket =
      socket
      |> assign(:selected_people, socket.assigns.selected_people -- [person])
      |> assign(:available_people_in_search, socket.assigns.available_people_in_search ++ [person])

    {:noreply, socket}
  end

  @impl true
  def handle_event("add_selected_people", _, socket) do
    for person <- socket.assigns.selected_people do
      Modalities.add_person_to_modality(socket.assigns.modality, person)
    end

    send(self(), {:update, %{}})

    {:noreply, assign(socket, :selected_people, [])}
  end

  @impl true
  def handle_info({:person_selected, person}, socket) do
    socket =
      socket
      |> assign(:selected_people, socket.assigns.selected_people ++ [person])
      |> assign(:available_people_in_search, socket.assigns.avaliable_people_in_search -- [person])

    {:noreply, socket}
  end

  @impl true
  def handle_info({:update, opts}, socket) do
    params = merge_and_sanitize_params(socket, opts)
    path = Routes.modality_show_path(socket, :show, socket.assigns.modality.id, params)

    {:noreply, push_patch(socket, to: path, replace: true)}
  end

  defp assign_modality(socket, %{"modality_id" => id}) do
    params = merge_and_sanitize_params(socket)
    %{modality: modality, total_people_count: total_people_count} =
      Modalities.get_modality_with_people_total_count!(id, params)

    socket
    |> assign(:modality, modality)
    |> assign(:people, modality.people)
    |> assign_total_count(total_people_count)
  end

  defp page_title(:show), do: gettext("Show Modality")
  defp page_title(:edit), do: gettext("Edit Modality")
  defp page_title(:add_people), do: gettext("Add People")

  defp parse_params(socket, params) do
    with {:ok, sorting_opts} <- SortingForm.parse(params),
         {:ok, filter_opts} <- FilterForm.parse(params),
         {:ok, pagination_opts} <- PaginationForm.parse(params) do
      socket
      |> assign_sorting(sorting_opts)
      |> assign_filter(filter_opts)
      |> assign_pagination(pagination_opts)
    else
      _error ->
        socket
        |> assign_sorting()
        |> assign_filter()
        |> assign_pagination()
    end
  end

  defp merge_and_sanitize_params(socket, overrides \\ %{}) do
    %{sorting: sorting, pagination: pagination, filter: filter} = socket.assigns
    overrides = maybe_reset_pagination(overrides)

    %{}
    |> Map.merge(sorting)
    |> Map.merge(pagination)
    |> Map.merge(filter)
    |> Map.merge(overrides)
    |> Map.drop([:total_count])
    |> Enum.reject(fn {_key, value} -> is_nil(value) end)
    |> Map.new()
  end

  defp assign_sorting(socket, overrides \\ %{}) do
    assign(socket, :sorting, SortingForm.default_values(overrides))
  end

  defp assign_pagination(socket, overrides \\ %{}) do
    assign(socket, :pagination, PaginationForm.default_values(overrides))
  end

  defp assign_filter(socket, overrides \\ %{}) do
    assign(socket, :filter, FilterForm.default_values(overrides))
  end

  defp assign_total_count(socket, total_count) do
    update(socket, :pagination, fn pagination -> %{pagination | total_count: total_count} end)
  end

  defp maybe_reset_pagination(overrides) do
    if FilterForm.contains_filter_values?(overrides) do
      Map.put(overrides, :page, 1)
    else
      overrides
    end
  end
end
