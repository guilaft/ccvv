defmodule CCVV.Users.Person do
  use Ecto.Schema
  import Ecto.Changeset
  alias Ecto.Changeset
  alias ExPhoneNumber

  alias CCVV.Modatilies.PeopleModalities

  @email_regex ~r"^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"

  schema "people" do
    field :details, :string
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :phone_number, :string

    many_to_many :modalities, CCVV.Modalities.Modality, join_through: PeopleModalities

    timestamps()
  end

  @doc false
  def changeset(person, attrs) do
    person
    |> cast(attrs, [:first_name, :last_name, :email, :phone_number, :details])
    |> validate_required([:first_name, :last_name])
    |> validate_email()
    |> validate_phone_number()
  end

  defp validate_email(changeset) do
    changeset
    |> validate_format(:email, @email_regex, message: "must be a valid email address")
  end

  defp validate_phone_number(%Changeset{changes: %{phone_number: phone_number}} = changeset) do
      case ExPhoneNumber.parse(phone_number, "BR") do
        {:ok, phone_number} ->
          Changeset.put_change(changeset, :phone_number, ExPhoneNumber.format(phone_number, :international))
        {:error, _} ->
          Changeset.add_error(changeset, :phone_number, "must be a valid phone number")
      end
  end

  defp validate_phone_number(changeset), do: changeset

 end
