defmodule CCVV.Modalities.Modality.Query do
  alias CCVV.Modalities.Modality

  import Ecto.Query

  def base, do: from m in Modality

  def with_people(query \\ base()) do
    query
    |> preload(:people)
  end
end
