defmodule CCVVWeb.PersonLiveTest do
  use CCVVWeb.ConnCase

  import Phoenix.LiveViewTest
  import CCVV.UsersFixtures

  @create_attrs %{details: "some details", email: "some@email.com", first_name: "some first_name", last_name: "some last_name", phone_number: "+1 234-567-890"}
  @update_attrs %{details: "some updated details", email: "some@updated.email", first_name: "some updated first_name", last_name: "some updated last_name", phone_number: "+1 234-234-567"}
  @invalid_attrs %{details: nil, email: nil, first_name: nil, last_name: nil, phone_number: nil}

  defp create_person(_) do
    person = person_fixture()
    %{person: person}
  end

  describe "Index" do
    setup [:create_person]

    test "lists all people", %{conn: conn, person: person} do
      {:ok, _index_live, html} = live(conn, Routes.person_index_path(conn, :index))

      assert html =~ "Listing People"
      assert html =~ person.details
    end

    test "saves new person", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.person_index_path(conn, :index))

      assert index_live |> element("a", "New Person") |> render_click() =~
               "New Person"

      assert_patch(index_live, Routes.person_index_path(conn, :new))

      assert index_live
             |> form("#person-form", person: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#person-form", person: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.person_index_path(conn, :index))

      assert html =~ "Person created successfully"
      assert html =~ "some details"
    end

    test "updates person in listing", %{conn: conn, person: person} do
      {:ok, index_live, _html} = live(conn, Routes.person_index_path(conn, :index))

      assert index_live |> element("#person-#{person.id} #person-edit-button") |> render_click() =~
               "Edit Person"

      assert_patch(index_live, Routes.person_index_path(conn, :edit, person))

      assert index_live
             |> form("#person-form", person: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#person-form", person: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.person_index_path(conn, :index))

      assert html =~ "Person updated successfully"
      assert html =~ "some updated details"
    end

    test "deletes person in listing", %{conn: conn, person: person} do
      {:ok, index_live, _html} = live(conn, Routes.person_index_path(conn, :index))

      assert index_live |> element("#person-#{person.id} #person-delete-button") |> render_click()
      refute has_element?(index_live, "#person-#{person.id}")
    end

    test "filters person by first and last name in listing", %{conn: conn} do
      person_1 = person_fixture(%{first_name: "Robert", last_name: "De Niro"})
      person_2 = person_fixture(%{first_name: "Willem", last_name: "Dafoe"})
      person_3 = person_fixture(%{first_name: "Jack", last_name: "Dafoe"})

      {:ok, index_live, _html} = live(conn, Routes.person_index_path(conn, :index))

      assert has_element?(index_live, "tr", person_1.first_name)
      assert has_element?(index_live, "tr", person_2.first_name)
      assert has_element?(index_live, "tr", person_3.first_name)

      index_live
      |> form("#table-filter", %{"filter[first_name]": "Willem"})
      |> render_submit()

      refute has_element?(index_live, "tr", person_1.first_name)
      assert has_element?(index_live, "tr", person_2.first_name)
      refute has_element?(index_live, "tr", person_3.first_name)

      {:ok, index_live, _html} = live(conn, Routes.person_index_path(conn, :index))

      index_live
      |> form("#table-filter", %{"filter[last_name]": "Dafoe"})
      |> render_submit()

      refute has_element?(index_live, "tr", person_1.first_name)
      assert has_element?(index_live, "tr", person_2.first_name)
      assert has_element?(index_live, "tr", person_3.first_name)
    end
  end

  describe "Show" do
    setup [:create_person]

    test "displays person", %{conn: conn, person: person} do
      {:ok, _show_live, html} = live(conn, Routes.person_show_path(conn, :show, person))

      assert html =~ "Show Person"
      assert html =~ person.details
    end

    test "updates person within modal", %{conn: conn, person: person} do
      {:ok, show_live, _html} = live(conn, Routes.person_show_path(conn, :show, person))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Person"

      assert_patch(show_live, Routes.person_show_path(conn, :edit, person))

      assert show_live
             |> form("#person-form", person: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#person-form", person: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.person_show_path(conn, :show, person))

      assert html =~ "Person updated successfully"
      assert html =~ "some updated details"
    end
  end
end
