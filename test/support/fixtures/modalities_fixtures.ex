defmodule CCVV.ModalitiesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `CCVV.Modalities` context.
  """

  @doc """
  Generate a modality.
  """
  def modality_fixture(attrs \\ %{}) do
    {:ok, modality} =
      attrs
      |> Enum.into(%{
        description: "some description",
        name: "some name"
      })
      |> CCVV.Modalities.create_modality()

    modality
  end
end
