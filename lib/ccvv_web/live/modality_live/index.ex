defmodule CCVVWeb.ModalityLive.Index do
  use CCVVWeb, :live_view

  alias CCVV.Modalities
  alias CCVV.Modalities.Modality

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :modalities, list_modalities())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, gettext("Edit Modality"))
    |> assign(:modality, Modalities.get_modality!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, gettext("New Modality"))
    |> assign(:modality, %Modality{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, gettext("Listing Modalities"))
    |> assign(:modality, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    modality = Modalities.get_modality!(id)
    {:ok, _} = Modalities.delete_modality(modality)

    {:noreply, assign(socket, :modalities, list_modalities())}
  end

  defp list_modalities do
    Modalities.list_modalities()
  end

  defp dropdown(assigns) do
    ~H"""
    <div id={"modality-#{@modality.id}-dropdown"} class="dropdown">
      <div class="dropdown-trigger">
        <button class="button" aria-haspopup="true" aria-controls="dropdown-menu" phx-click={toggle_active("#modality-#{@modality.id}-dropdown")}>
          <span class="icon is-small">
            <.icon name="bars-3"/>
          </span>
        </button>
      </div>
      <div class="dropdown-menu" id="dropdown-menu" role="menu">
        <div class="dropdown-content">
          <%= live_redirect gettext("Show"), to: Routes.modality_show_path(@socket, :show, @modality), class: "dropdown-item" %>
          <%= live_patch gettext("Edit"), to: Routes.modality_index_path(@socket, :edit, @modality), class: "dropdown-item" %>
          <%= link gettext("Delete"), to: "#", phx_click: "delete", phx_value_id: @modality.id, data: [confirm: "Are you sure?"], class: "dropdown-item" %>
        </div>
      </div>
    </div>
    """
  end
end
