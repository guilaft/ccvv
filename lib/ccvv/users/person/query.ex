defmodule CCVV.Users.Person.Query do
  alias CCVV.Users.Person

  import Ecto.Query

  def base, do: from p in Person

  def list_all(query \\ base(), criteria) when is_list(criteria) do
    Enum.reduce(criteria, query, fn
      {:paginate, %{page: page, per_page: per_page}}, query ->
        from q in query,
          offset: ^((page - 1) * per_page),
          limit: ^per_page

      {:sort, %{sort_by: sort_by, sort_order: sort_order}}, query ->
        from q in query, order_by: [{^sort_order, ^sort_by}]
    end)
  end

  def sort(query \\ base(), _)

  def sort(query, %{sort_dir: sort_dir, sort_by: sort_by})
       when sort_dir in [:asc, :desc] and
              sort_by in [:id, :first_name, :last_name] do
    order_by(query, {^sort_dir, ^sort_by})
  end

  def sort(query, _opts), do: query

  def paginate(query \\ base(), _)

  def paginate(query, %{page: page, page_size: page_size})
       when is_integer(page) and is_integer(page_size) do
    offset = max(page - 1, 0) * page_size

    query
    |> limit(^page_size)
    |> offset(^offset)
  end

  def paginate(query, _opts), do: query

  def filter(query \\ base(), opts) do
    query
    |> filter_by_id(opts)
    |> filter_by_first_name(opts)
    |> filter_by_last_name(opts)
  end

  def filter_by_id(query \\ base(), _)

  def filter_by_id(query, %{id: id}) when is_integer(id) do
    where(query, id: ^id)
  end

  def filter_by_id(query, _opts), do: query

  def filter_by_first_name(query \\ base(), _)

  def filter_by_first_name(query, %{first_name: first_name})
       when is_binary(first_name) and first_name != "" do
    query_string = "%#{first_name}%"
    where(query, [p], ilike(p.first_name, ^query_string))
  end

  def filter_by_first_name(query, _opts), do: query

  def filter_by_last_name(query \\ base(), _)

  def filter_by_last_name(query, %{last_name: last_name})
       when is_binary(last_name) and last_name != "" do
    query_string = "%#{last_name}%"
    where(query, [p], ilike(p.last_name, ^query_string))
  end

  def filter_by_last_name(query, _opts), do: query
end
