defmodule CCVVWeb.PersonLive.Show do
  use CCVVWeb, :live_view

  alias CCVV.Users

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:person, Users.get_person!(id))}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    person = Users.get_person!(id)
    {:ok, _} = Users.delete_person(person)

    {:noreply, redirect(socket, to: Routes.person_index_path(socket, :index))}
  end

  defp page_title(:show), do: "Show Person"
  defp page_title(:edit), do: "Edit Person"
end
